"use strict";

const pg = require("pg");
const valueSequelize = require("sequelize");

const client = new pg.Client({
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  port: process.env.POSTGRES_POSTGRES_PORT,
});

client
  .connect()
  .then((db) => {
    console.log("DB pg is connect");
    console.log("************************************");
  })
  .catch((err) => console.error(err));

const sequelize = new valueSequelize(
  process.env.POSTGRES_DB,
  process.env.POSTGRES_USER,
  process.env.POSTGRES_PASSWORD,
  {
    host: process.env.POSTGRES_HOST,
    dialect: "postgres",
    port: process.env.POSTGRES_POSTGRES_PORT,
    dialectOptions: {
      connectTimeout: 60000,
    },
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000,
    },
  }
);

const db = {};

db.valueSequelize = valueSequelize;
db.sequelize = sequelize;

db.user = require("./src/models/user.model")(sequelize, valueSequelize);
db.project = require("./src/models/project.model")(sequelize, valueSequelize);
db.category = require("./src/models/category.model")(sequelize, valueSequelize);
db.skill = require("./src/models/skill.model")(sequelize, valueSequelize);
db.image = require("./src/models/image.model")(sequelize, valueSequelize);
db.role = require("./src/models/role.model")(sequelize, valueSequelize);

db.user_role = require("../server/src/models/user_role.model")(
  sequelize,
  valueSequelize
);
db.project_user = require("../server/src/models/project_user.model")(
  sequelize,
  valueSequelize
);
db.project_category = require("../server/src/models/project_category.model")(
  sequelize,
  valueSequelize
);
db.user_skill = require("../server/src/models/user_skill.model")(
  sequelize,
  valueSequelize
);

// relationship many to many

db.user.belongsToMany(db.role, {
  through: db.user_role,
  onDelete: "CASCADE",
  foreignKey: "user_id",
});
db.role.belongsToMany(db.user, {
  through: db.user_role,
  onDelete: "CASCADE",
  foreignKey: "role_id",
});

db.project.belongsToMany(db.user, {
  through: db.project_user,
  onDelete: "CASCADE",
  foreignKey: "project_id",
});
db.user.belongsToMany(db.project, {
  through: db.project_user,
  onDelete: "CASCADE",
  foreignKey: "user_id",
});

db.user.belongsToMany(db.skill, {
  through: db.user_skill,
  onDelete: "CASCADE",
  foreignKey: "user_id",
});
db.skill.belongsToMany(db.user, {
  through: db.user_skill,
  onDelete: "CASCADE",
  foreignKey: "skill_id",
});

db.project.belongsToMany(db.category, {
  through: db.project_category,
  onDelete: "CASCADE",
  foreignKey: "project_id",
});
db.category.belongsToMany(db.project, {
  through: db.project_category,
  onDelete: "CASCADE",
  foreignKey: "category_id",
});

// relationship one to many

db.project.hasMany(db.image, { onDelete: "CASCADE" });
db.image.belongsTo(db.project, { onDelete: "CASCADE" });

module.exports = db;

'user strict'

const bcrypt = require('bcrypt');
const db = require('../../db');
const User = db.user;
const Category = db.category;
const Project = db.project;
const Role = db.role;
const UserRole = db.user_role;

const dataUser = [
    {
        id: 1,
        firstName: 'Ayar Iru',
        lastName: 'Perez Vargas',
        userName: 'mos1234',
        email: 'xhosone@gmail.com',
        password: '12345',
        status: true,
        gender: 'MALE',
        birthDate: new Date(1980, 6, 20),
        phone: '75224832'
    },
    {
        id: 2,
        firstName: 'admin',
        lastName: 'admin',
        userName: 'admin',
        email: 'admin@localhost.com',
        password: '12345',
        status: true,
        gender: 'MALE',
        birthDate: new Date(1980, 6, 20)
    }
];
const dataRole = [
    {
        id: 1,
        name: 'ADMINISTRATOR'
    },
    {
        id: 2,
        name: 'USER'
    }
];
const data_user_role = [
    {
        user_id: 1,
        role_id: 2
    },
    {
        user_id: 2,
        role_id: 1
    },
    {
        user_id: 2,
        role_id: 2
    }
]
const dataCategory = [
    {
        title: 'WEB',
        description: 'Aplication web'
    },
    {
        title: 'ANDROID',
        description: 'Aplication android'
    },
    {
        title: 'HTML',
        description: 'HTML CSS'
    },
    {
        title: 'BACKEND',
        description: 'backend'
    },
    {
        title: 'FRONTEND',
        description: 'frontend'
    }
];
const dataProject = [
    {
        title: 'Project1', 
        name: 'Project1',
        description: 'Project1',
        status: true
    },
    {
        title: 'Project2', 
        name: 'Project2',
        description: 'Project2',
        status: true
    },
    {
        title: 'Project3', 
        name: 'Project3',
        description: 'Project3',
        status: true
    },
    {
        title: 'Project4', 
        name: 'Project4',
        description: 'Project4',
        status: true
    }
];

exports.initialDataUser = function () {
    dataUser.forEach(user => {
        user.password = bcrypt.hashSync(user.password, 10);
        User.create(user);
    });
    dataRole.forEach(role => {
        Role.create(role);
    });
    dataCategory.forEach(category => {
        Category.create(category);
    });
    dataProject.forEach(project => {
        Project.create(project);
    });
    data_user_role.forEach(ur => {
        UserRole.create(ur)
    });
}

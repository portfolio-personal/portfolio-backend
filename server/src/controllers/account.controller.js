'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const User = db.user;
const Role = db.role;
const Sequelize = require('sequelize');
const cryptoRandomString = require('crypto-random-string');
const mail = require('../_helpers/mails/mail');
const auth = require('../services/auth.service');

const accountCtrl = {};

//register
// accountCtrl.signup = async (req, res) => {
//     const data = await Usuario.findOne({ where: { userName: req.body.userName, email: req.body.email } });
//     try {
//         if (data !== null) {
//             res.status(400).json('User already exists');
//         } else {
//             const refreshToken = cryptoRandomString({ length: 50, characters: '1234567890abcdefghijklmnopqrstuvwxyz' });
//             const new_User = {
//                 nombres: req.body.nombres,
//                 apellidos: req.body.apellidos,
//                 userName: req.body.userName,
//                 email: req.body.email,
//                 password: bcrypt.hashSync(req.body.password, 10),
//                 rol: ['ROL_CLIENTE'],
//                 genero: 'MASCULINO',
//                 estado: false,
//                 activate_key: refreshToken,
//             }
//             Usuario.create(new_User).
//                 then((usuario) => {
//                     const data = usuario.dataValues;
//                     const url = 'http://localhost:8080/activate-account?key=' + refreshToken;
//                     mail.sendMail('/templates/activateAccount.html', 'Activar Cuenta', url, data);
//                     res.status(200).json({ status: 201, msg: 'created' });
//                 }).catch((err) => {
//                     const dataError = Object.assign({}, err.errors['0']);
//                     res.status(500).json(
//                         {
//                             msg: 'error create user',
//                             details: dataError
//                         });
//                 });
//         }
//     } catch (error) {
//         const dataError = Object.assign({}, err.errors['0']);
//         res.status(500).json(
//             {
//                 msg: 'error to the create user',
//                 details: dataError
//             });
//     }
// }

// login
accountCtrl.login = async (req, res) => {
    try {
        const response = await User.findOne({ where: { email: req.body.email }, include: [{ model: Role }] });
        if (!response.dataValues.status) {
            res.status(500).send({ msg: 'The account is not activate' });
        } else {
            if (bcrypt.compareSync(req.body.password, response.dataValues.password)) {
                const token = auth.createToken(response.dataValues);
                console.log('Its token to created --> ', token);
                res.status(200).json({ token: token });
            } else {
                res.status(400).send({ msg: 'Password incorrect' });
            }
        }
    } catch (error) {
        res.status(500).send({msg: 'error', details: 'Email o Password is incorrect'});
    }
}

accountCtrl.account = async (req, res) => {
    try {
        const data = await auth.showInsideToken(req.token);
        res.status(200).json(data);
    } catch (err) {
        res.status(500).json({ msg: 'Error token expired', details: err })
    }
}

// accountCtrl.logout = async (req, res) => {
//     const data = await auth.destroyToken(req.token);
//     res.json(data);
// }

// accountCtrl.resetPasswordInit = (req, res) => {
//     const email = req.body.email;
//     const refreshToken = cryptoRandomString({ length: 50, type: 'base64' });
//     User.findOne({ where: { email: email } }).then((user) => {
//         const url = 'http://localhost:8080/reset-password-finish/';
//         mail.sendMail('/html/resetPassword.html', 'Recuperar Contraseña', url + refreshToken, user);
//         res.status(200).json({ key: "Esta es la llave que se mandara" });
//         const data = user.dataValues;
//         data.reset_key = refreshToken;
//         User.update(data, { where: { id: user.id } }).then(() => {
//             console.log('Se le envio una URL');
//             res.status(200).json('Your account was updated successfully');
//         });
//     }).catch((err) => {
//         res.status(500).json({ msg: 'error', details: err });
//     });
// }

// accountCtrl.resetPasswordFinish = (req, res) => {
//     const key = req.body.key;
//     const newPassword = req.body.newPassword;
//     User.findOne({ where: { reset_key: key } }).
//         then((user) => {
//             bcrypt.hash(newPassword, 10, function (err, hash) {
//                 const data = user.dataValues;
//                 data.reset_key = null;
//                 data.password = hash;
//                 User.update(data, { where: { id: data.id } }).then(() => {
//                     console.log('La contrasenia fue actualizada exitosamente');
//                     res.status(200).json('Your account was updated successfully');
//                 });
//             });
//         }).catch((err) => {
//             res.status(500).json({ msg: 'error', details: err });
//         });
// }

// accountCtrl.activateAccount = async (req, res, next) => {
//     const key = req.body.key;
//     const findByKey = await Usuario.findOne({ where: { activate_key: key } });
//     if (findByKey !== null) {
//         const data = await findByKey.dataValues;
//         data.estado = true;
//         data.activate_key = null;
//         await Usuario.update(data, { where: { id: findByKey.id } })
//         res.status(200).json(findByKey);
//     } else {
//         res.status(500).json({ msg: 'error', details: 'La llave no es valida' });
//     }
// }

module.exports = accountCtrl;
'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const Category = db.category;
const Sequelize = require('sequelize');

const categoryCtrl = {};

categoryCtrl.findAll = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    try {
        const category = await Category.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]] }); 
        const convert = category.count / 10;
        const pages = convert > Math.round(convert) ? Math.round(convert) + 1 : Math.round(convert);
        const elements = category.count;
        const rows = category.rows;
        res.status(200).json(
            {
                elements,
                page,
                pageSize,
                pages,
                rows
            }
        );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

categoryCtrl.create = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    req.body.password = '12345';
    console.log('READ DATA', req.body);
    try {
        const response = await Category.create(req.body);
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

categoryCtrl.update = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    try {
        const [numberOfAffectedRows, affectedRows] = await Category.update(datas, { where: { id: datas.id }, returning: true, plain: true }); 
        res.status(200).json(affectedRows.dataValues);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

categoryCtrl.findById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await Category.findOne({ where: { id: id }});
        res.status(200).json(response);
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

categoryCtrl.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Category.destroy({ where: { id: id } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

categoryCtrl.search = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const keyword = req.query.keyword ? req.query.keyword.toUpperCase() : '';
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    let queryOne = `UPPER("title") like '%${keyword}%'`;
    let searchByRoleOrKeyword = Sequelize.literal(`${queryOne}`);
    try {
        const category = await Category.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]], where: { searchByRoleOrKeyword } }); 
        const convert = category.count / 10;
        const pages = convert > Math.round(convert) ? Math.round(convert) + 1 : Math.round(convert);
        const elements = category.count;
        const rows = category.rows;
        res.status(200).json(
            {
                elements,
                page,
                pageSize,
                pages,
                rows
            }
        );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

module.exports = categoryCtrl;
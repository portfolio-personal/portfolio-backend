'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const Image = db.image;
const Sequelize = require('sequelize');

const imageCtrl = {};

imageCtrl.findAll = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    try {
        const image = await Image.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]] }); 
        const pages = Math.ceil(image.count / limit);
        const elements = image.count;
        const rows = image.rows;
        res.status(200).json(
            {
                elements,
                page,
                pageSize,
                pages,
                rows,
            }
        );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

imageCtrl.findAllByProjectId = async (req, res) => {
    try {
        const image = await Image.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]] }); 
        const pages = Math.ceil(image.count / limit);
        const elements = image.count;
        const rows = project.rows;
        res.status(200).json(
            {
                elements,
                page,
                pageSize,
                pages,
                rows,
            }
        );
    } catch (error) {
        
    }
}

imageCtrl.create = async (req, res) => {
    try {
        const data = {
            image: `http://localhost:2100${req.file.path}`,
            name: req.body.name,
            type: req.body.type,
            projectId: req.body.projectId
        }
        const response = await Image.create(data);
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

imageCtrl.update = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    try {
        const [numberOfAffectedRows, affectedRows] = await Image.update(datas, { where: { id: datas.id }, returning: true, plain: true }); 
        res.status(200).json(affectedRows.dataValues);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

imageCtrl.findById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await Image.findOne({ where: { id: id }});
        res.status(200).json(response);
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

imageCtrl.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Image.destroy({ where: { id: id } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

module.exports = imageCtrl;
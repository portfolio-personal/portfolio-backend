'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const Project = db.project;
const User = db.user;
const Category = db.category;
const ProjectUser = db.project_user;
const ProjectCategory = db.project_category;
const Sequelize = require('sequelize');

const projectCtrl = {};

projectCtrl.findAll = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    console.log('=-------------->>>>>>> ', page)
    try {
        const project = await Project.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]], include: { all: true }, });
        const convert = project.count / 10;
        const pages = convert > Math.round(convert) ? Math.round(convert) + 1 : Math.round(convert);
        console.log('Hello', project);
        // const pages = Math.ceil(project.count / limit);
        const elements = project.count;
        const rows = project.rows;
        // res.setHeader('x-total-count', elements);
        res.status(200).json(
            {
                elements,
                page,
                pageSize,
                pages,
                rows
            }
        );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

projectCtrl.create = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    req.body.password = '12345';
    console.log('READ DATA', req.body);
    try {
        const response = await Project.create(req.body);
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

projectCtrl.update = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    try {
        const [numberOfAffectedRows, affectedRows] = await Project.update(datas, { where: { id: datas.id }, returning: true, plain: true, include: { all: true } }); 
        res.status(200).json(affectedRows.dataValues);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

projectCtrl.findById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await Project.findOne({ where: { id: id }, include: { all: true }});
        res.status(200).json(response);
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

projectCtrl.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Project.destroy({ where: { id: id } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

// Added project user relationship

projectCtrl.addProjectUser = async (req, res) => {
    try {
        console.log('---> ', req.body);
        const response = await ProjectUser.create({ project_id: req.body.projectId, user_id: req.body.userId });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

projectCtrl.removeProjectUser = async (req, res) => {
    try {
        await ProjectUser.destroy({ where: { userId: id, projectId: id } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

// Added category

projectCtrl.addCategory = async (req, res) => {
    console.log('this is the body --> ', req.body);
    try {
        const response = await ProjectCategory.create({ project_id: req.body.projectId, category_id: req.body.categoryId });
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

projectCtrl.removeCategory = async (req, res) => {
    console.log('this is the body --> ', req.params);
    try {
        const response = await ProjectCategory.destroy({ where: { project_id: req.params.projectId, category_id: req.params.categoryId } })
        await res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id: req.params.categoryId });
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

module.exports = projectCtrl;
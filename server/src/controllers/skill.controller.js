'use strict'

const db = require('../../db');
const bcrypt = require('bcrypt');
const control = require('../_helpers/pagination');
const Skill = db.skill;
const Sequelize = require('sequelize');

const skillCtrl = {};

skillCtrl.findAll = async (req, res) => {
    const page = req.query.page ? parseInt(req.query.page) : 0;
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    const offset = page * pageSize;
    const limit = offset + pageSize;
    const value = req.query.sort ? req.query.sort : 'id';
    const type = req.query.type ? req.query.type.toUpperCase() : 'ASC';
    try {
        const skill = await Skill.findAndCountAll({ offset: parseInt(offset), limit: parseInt(pageSize), order: [[value, type]] }); 
        const pages = Math.ceil(skill.count / limit);
            const elements = user.count;
            res.status(200).json(
                {
                    elements,
                    page,
                    pageSize,
                    pages,
                    skill,
                }
            );
    } catch (error) {
        res.status(500).json({ msg: "error", details: err });
    }
}

skillCtrl.create = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    req.body.password = '12345';
    console.log('READ DATA', req.body);
    try {
        const response = await Skill.create(req.body);
        res.status(200).json(response);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

skillCtrl.update = async (req, res) => {
    const datas = await Object.assign({}, req.body);
    try {
        const [numberOfAffectedRows, affectedRows] = await Skill.update(datas, { where: { id: datas.id }, returning: true, plain: true }); 
        res.status(200).json(affectedRows.dataValues);
    } catch (error) {
        res.status(500).json({ msg: 'error', details: error });
    }
}

skillCtrl.findById = async (req, res) => {
    const id = req.params.id;
    try {
        const response = await Skill.findOne({ where: { id: id }});
        res.status(200).json(response);
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

skillCtrl.delete = async (req, res) => {
    const id = req.params.id;
    try {
        await Skill.destroy({ where: { id: id } })
        res.status(200).json({ msg: 'deleted successfully -> curstomer id = ', id });
    } catch (error) {
        res.status(300).json({ msg: 'error', details: err });
    }
}

module.exports = skillCtrl;
'use strict'

module.exports = (sequelize, Sequelize) => {
    const category = sequelize.define('category', {
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        }
    });
    return category
}
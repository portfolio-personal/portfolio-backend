'use strict'

module.exports = (sequelize, Sequelize) => {
    const project = sequelize.define('project', {
        name: {
            type: Sequelize.STRING
        },
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.BOOLEAN
        }
    });
    return project;
}
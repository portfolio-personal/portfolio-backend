'use strict'

module.exports = (sequelize, Sequelize) => {
    const role = sequelize.define('role', {
        name: {
            type: Sequelize.STRING
        }
    });
    return role;
}

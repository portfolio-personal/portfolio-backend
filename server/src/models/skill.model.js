'use strict'

module.exports = (sequelize, Sequelize) => {
    const skill = sequelize.define('skill', {
        name: {
            type: Sequelize.STRING
        },
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        status: {
            type: Sequelize.BOOLEAN
        }
    });
    return skill;
}
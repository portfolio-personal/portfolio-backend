'use strict'

module.exports = (sequelize, Sequelize) => {
    const skill = sequelize.define('skill', {
        title: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        }
    });
    return skill;
}
'user strict'

module.exports = (sequealize, Sequealize) => {
    const user = sequealize.define('user', {
        firstName: {
            type: Sequealize.STRING
        },
        middleName: {
            type: Sequealize.STRING
        },
        lastName: {
            type: Sequealize.STRING
        },
        userName: {
            type: Sequealize.STRING,
            unique: {
                args: true,
                msg: 'username already exists'
            },
            allowNull: false,
            validate: {
                len: {
                    args: [3, 100],
                    msg: 'Porfavor el userName tinene que tener mas de 3 caracteres y menos de 100'
                }
            }
        },
        email: {
            type: Sequealize.STRING,
            unique: {
                args: true,
                msg: 'Email already exists'
            },
            allowNull: false,
            validate: {
                notEmpty: {
                    args: true,
                    msg: 'Emails-id required'
                },
                isEmail: {
                    args: true,
                    msg: 'Valid email-id required'
                }
            }
        },
        password: {
            type: Sequealize.STRING,
            allowNull: false,
            validate: {
                len: {
                    args: [4, 100],
                    msg: 'Porfavor el password tinene que tener >= a 4 caracteres y <= que 100 caracteres'
                }
            }
        },
        image: {
            type: Sequealize.STRING(255)
        },
        type: {
            type: Sequealize.STRING(255)
        },
        name: {
            type: Sequealize.STRING(255)
        },
        role: {
            type: Sequealize.ARRAY(Sequealize.STRING)
        },
        gender: {
            type: Sequealize.ENUM('MALE', 'FEMALE')
        },
        description: {
            type: Sequealize.TEXT
        },
        status: {
            type: Sequealize.BOOLEAN
        },
        birthdate: {
            type: Sequealize.DATEONLY()
        },
        phone: {
            type: Sequealize.STRING
        },
        activate_key: {
            type: Sequealize.STRING
        },
        reset_key: {
            type: Sequealize.STRING
        },
    });
    return user;
}
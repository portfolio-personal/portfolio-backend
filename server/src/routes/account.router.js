'use strict'

const express = require('express');
const router = express.Router();

const accountCtrl = require('../controllers/account.controller');

router.post('/api/login', accountCtrl.login);
router.get('/api/account', accountCtrl.account);

module.exports = router;
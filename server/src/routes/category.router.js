'use strict'

const express = require('express');
const router =  express.Router();

const categoryCtrl = require('../controllers/category.controller');

router.get('/api/categories', categoryCtrl.findAll);
router.post('/api/categories', categoryCtrl.create);
router.put('/api/categories', categoryCtrl.update);
router.get('/api/categories/:id', categoryCtrl.findById);
router.delete('/api/categories/:id', categoryCtrl.delete);
router.get('/api/search-by-category', categoryCtrl.search);

module.exports = router;
'use strict'

const express = require('express');
const router = express.Router();
const multer = require('multer');

const imageCtrl = require('../controllers/image.controller');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp/my-uploads')
        // cb(null, '././uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
})

var upload = multer({ storage: storage })

router.get('/api/images', imageCtrl.findAll);
router.post('/api/images', upload.single('uploadImage'), imageCtrl.create);
router.put('/api/images', upload.single('uploadImage'), imageCtrl.update);
router.get('/api/images/:id', imageCtrl.findById);
router.delete('/api/images/:id', imageCtrl.delete);

module.exports = router;
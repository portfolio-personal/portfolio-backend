'use strict'

const express = require('express');
const router =  express.Router();

const projectCtrl = require('../controllers/project.controller');

router.get('/api/projects', projectCtrl.findAll);
router.post('/api/projects', projectCtrl.create);
router.put('/api/projects', projectCtrl.update);
router.get('/api/projects/:id', projectCtrl.findById);
router.delete('/api/projects/:id', projectCtrl.delete);
router.post('/api/add-project-user', projectCtrl.addProjectUser);
router.delete('/api/remove-project-user/:id', projectCtrl.removeProjectUser);
router.post('/api/add-category', projectCtrl.addCategory);
router.delete('/api/remove-category/:projectId/:categoryId', projectCtrl.removeCategory);

module.exports = router;
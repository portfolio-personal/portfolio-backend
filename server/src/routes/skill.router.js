'use strict'

const express = require('express');
const router =  express.Router();

const skillCtrl = require('../controllers/skill.controller');

router.get('/api/skills', skillCtrl.findAll);
router.post('/api/skills', skillCtrl.create);
router.put('/api/skills', skillCtrl.update);
router.get('/api/skills/:id', skillCtrl.findById);
router.delete('/api/skills/:id', skillCtrl.delete);

module.exports = router;
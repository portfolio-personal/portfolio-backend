'use strict'

const express = require('express');
const router = express.Router();

const userCtrl = require('../controllers/user.controller');

router.get('/api/users', userCtrl.findAll);
router.post('/api/users', userCtrl.create);
router.put('/api/users', userCtrl.update);
router.get('/api/users/:id', userCtrl.findById);
router.delete('/api/users/:id', userCtrl.delete);

module.exports = router;